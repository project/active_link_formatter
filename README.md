Active Link Formatter Module
===========================

Description
-----------
Active Link Formatter is a Drupal module that provides a custom Formatter for
Link field types. Once enabled and configured, this module adds the "is-active"
class to the link if the current URL matches the one set on it.

By default, link field types do not utilize this feature, and there is no
built-in way to enable it. While it makes sense to have this feature disabled in
most cases, there are scenarios where having it active is desirable.

This module primarily functions by adding the 'set_active_class' parameter to
the link render array, which is then considered by the link_generator service.
It relies on the implementation provided by Drupal Core, specifically the
\Drupal\Core\Utility\LinkGenerator::generate() method.

Why Use This Module
-------------------
In typical cases, applying the "is-active" class to field links is unnecessary,
especially for site navigation menus, which are usually implemented using Drupal
menus. Drupal automatically adds the 'is-active' class to menus when needed.

However, there are situations where you may want links to have special styles if
you are on the same page to which the
link points. This module allows you to address these specific cases.

How to Use It
--------------

1. **Enable the Module:**
  Install and enable the Active Link Formatter module like any other module in
  Drupal.
2. **Configure Field Display:**
  Go to your content type > View Display.
  Apply the "Active Link" formatter to the desired field.

3. **Formatter Settings:**
  Within the formatter settings, enable the option "Process the links to add
  the is-active class if needed."

4. **Save Changes:**
  Save your changes to apply the Active Link Formatter to the selected field.

Contributing
-------------
Contributions are welcome! If you encounter issues or have suggestions for
improvements, please feel free to open an
issue on the
module's [issue queue](https://www.drupal.org/project/issues/3282682).

License
-------
This module is released under
the [GNU General Public License](https://www.gnu.org/licenses/gpl-2.0.html).
